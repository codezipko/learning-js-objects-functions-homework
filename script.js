var entryFieldModule = (function() {
    'use strict';

    var input,
        output,
        value;

    function _setInputValue() {
        value = input.value;
        output.innerHTML = value;
    }

    function callOutput(querySelectorInput, querySelectorOutput) {
        input = document.querySelector(querySelectorInput);
        output = document.querySelector(querySelectorOutput);
        input.addEventListener('keyup', _setInputValue);
    }

    return {
        callOutput: callOutput
    };
}());

entryFieldModule.callOutput('.input', '.message');